import dotenv from 'dotenv';
import express from 'express';
import { WebUntis } from 'webuntis';
import * as path from 'path';

// This is important for the .env Configuration file!
dotenv.config();

const __dirname = path.resolve();
const ip = process.env.IP;
const port = process.env.PORT;
const rIntervall = process.env.REFRESH_INTERVALL;

let data = [];

async function getDays(){
	let dateandtimetoday = new Date(),
        dd = String(dateandtimetoday.getDate()).padStart(2, "0"),
        mm = String(dateandtimetoday.getMonth() + 1).padStart(2, "0"),
        yyyy = dateandtimetoday.getFullYear(),
		hh = dateandtimetoday.getHours() + 2,
		min = String(dateandtimetoday.getMinutes()).padStart(2, "0");
	let datetoday = yyyy + "-" + mm + "-" + dd;
	let currentday = new Date(datetoday);
	let timetoday = hh + min;
	const endDate = new Date("2024-05-07");
	const schoolname = process.env.SCHOOL_NAME;
	const username = process.env.USERNAME;
	const password = process.env.PASSWORD;
	const link = process.env.SCHOOL_URL;

	const untis = new WebUntis(`${schoolname}`, `${username}`, `${password}`, `${link}`);
	await untis.login();
	const timetable = await untis.getOwnTimetableForRange(currentday, endDate);

	data = [];
	let dates = [];
	let bfkS = [];
	let bfkI = [];
	let bfkB = [];
	let wi = [];
	let gk = [];
	let deutsch = [];
	let englisch = [];
	for (let i = 0; i < timetable.length; i++) {
		if(timetable[i].date == Number(yyyy + mm + dd) && timetable[i].endTime > Number(timetoday) || timetable[i].date != Number(yyyy + mm + dd)) {
			if(!dates.includes(timetable[i].date) && timetable.code != "cancelled"){
				dates.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "BfK-S_E3FS5"){
				bfkS.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "BfK-I_E3FS5"){
				bfkI.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "BfK-B_E3FS5"){
				bfkB.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "WI_E3FS5"){
				wi.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "GK_E3FS5"){
				gk.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "D_E3FS5"){
				deutsch.push(timetable[i].date);
			}
			if(timetable.code != "cancelled" && timetable[i].sg == "E_E3FS5"){
				englisch.push(timetable[i].date);
			}
		}
	}
	data.push(dates.length);
	data.push(bfkS.length / 2);
	data.push(bfkI.length / 2);
	data.push(bfkB.length / 2);
	data.push(wi.length / 2);
	data.push(gk.length / 2);
	data.push(deutsch.length / 2);
	data.push(englisch.length / 2);
}

function intervalFunc() {
    getDays();
}
 setInterval(intervalFunc, rIntervall);

const app = express();

app.use(express.static(path.join(__dirname, '/src')));

app.get('/', async function(req, res) {
	res.sendFile(path.join(__dirname, '/src/main.html'));
	getDays();
});

 app.get('/data', async function (req, res) {
	res.json(data);
}); 

app.listen(port, ip);
console.log(`Server running at ${ip}:${port}`);