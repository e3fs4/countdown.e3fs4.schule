
(function () {
    const second = 1000,
          minute = second * 60,
          hour = minute * 60,
          day = hour * 24;
  
    //I'm adding this section so I don't have to keep updating this pen every year :-)
    //remove this if you don't need it
    let exams = "2024-05-07T10:30:00";
    
    const countDown = new Date(exams).getTime(),
        x = setInterval(function() {    
  
          const now = new Date().getTime(),
                distance = countDown - now;
  
          document.getElementById("days").innerText = Math.floor(distance / (day)),
            document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
            document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
            document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);
  
          //do something later when date is reached
          if (distance < 0) {
            document.getElementById("headline").innerText = "Viel Erfolg bei der Präsentation!";
            document.getElementById("countdown").style.display = "none";
            document.getElementById("content").style.display = "block";
            clearInterval(x);
          }
          //seconds
        }, 0)
}());

const schooldays = document.querySelector('#schooldays');
const hoursOfbfks = document.querySelector('#bfks');
const hoursOfbfki = document.querySelector('#bfki');
const hoursOfbfkb = document.querySelector('#bfkb');
const hoursOfwi = document.querySelector('#wi');
const hoursOfgk = document.querySelector('#gk');
const hoursOfdeutsch = document.querySelector('#deutsch');
const hoursOfenglisch = document.querySelector('#englisch');
const hourstotal = document.querySelector('#hourstotal');

fetch('https://countdown.fi.wern3r.de/data')
  .then(response => response.json())
  .then(data => {
    schooldays.insertAdjacentHTML("beforeend", data[0]);
    hoursOfbfks.insertAdjacentHTML("beforeend", data[1]);
    hoursOfbfki.insertAdjacentHTML("beforeend", data[2]);
    hoursOfbfkb.insertAdjacentHTML("beforeend", data[3]);
    hoursOfwi.insertAdjacentHTML("beforeend", data[4]);
    hoursOfgk.insertAdjacentHTML("beforeend", data[5]);
    hoursOfdeutsch.insertAdjacentHTML("beforeend", data[6]);
    hoursOfenglisch.insertAdjacentHTML("beforeend", data[7]);
    hourstotal.insertAdjacentHTML("beforeend", data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7]);

  })
  .catch(err => console.log(err));